import pluralize from 'pluralize'
import { useEffect, useState } from 'react'

import { LocationData } from '@/types'
import Loader from '@/components/loader'
import { useFetch, useToggle } from '@/hooks'
import Pagination from '@/components/pagination'
import Location from '@/components/drawers/location'

const Home: React.FC = () => {
    const [page, setPage] = useState<number>(1)
    const [pages, setPages] = useState<number>(1)
    const [selected, setSelected] = useState<LocationData>()
    const [str, setStr] = useState<string>('')
    const [charStr, setCharStr] = useState<string>('')
    const [epStr, setEpStr] = useState<string>('')
    const [charIds, setCharIds] = useState<string[]>([])
    const [epIds, setEpIds] = useState<string[]>([])
    const [ids, setIds] = useState<string>('')
    const [_ids, set_Ids] = useState<string>('')

    const [visible, toggle] = useToggle()

    const intersection = () => {
        let matches: string[] = []

        if (charIds?.length > 0 && epIds?.length > 0)
            matches = charIds.filter(val => epIds.includes(val))
        else if (charIds?.length > 0)
            matches = charIds
        else if (epIds?.length > 0)
            matches = epIds
        console.log({ epIds, charIds, matches })
        setPage(1)
        setIds(matches.map(str => {
            let split = str.split('/')
            return split[split.length - 1]
        }).join(','))
    }

    const { data, loading, error, execute } = useFetch(ids?.length > 0 ? `/location/${ids}` : `/location?page=${page}&name=${str}`, { method: 'GET' }, true, resp => {
        setPages(Array.isArray(resp) ? 1 : resp?.info?.pages)
    })

    const characters = useFetch(`/character?name=${charStr}`, { method: 'GET' }, true, resp => {
        if (charStr?.length > 0)
            setCharIds(resp?.results?.map((res: { location: { url: any } }) => res?.location?.url) || [])
        else
            setCharIds([])

        intersection()
    })

    const episodes = useFetch(`/episode?name=${epStr}`, { method: 'GET' }, true, resp => {
        console.log(resp)
        if (epStr?.length > 0)
            setEpIds(resp?.results?.map((res: { location: { url: any } }) => res?.location?.url) || [])
        else 
            setEpIds([])

        intersection()
    })

    useEffect(() => {
        execute()
        return () => { }
    }, [page, str, ids])

    useEffect(() => {
        characters.execute()
        return () => { }
    }, [charStr])

    useEffect(() => {
        episodes.execute()
        return () => { }
    }, [epStr])

    const select = (res: LocationData) => {
        let _id = res.residents.map(res => {
            const parts = res.split('/')
            return parts[parts.length - 1]
        }).join(',')
        set_Ids(_id)

        setSelected(res)
        toggle()
    }

    const clear = () => {
        setCharIds([])
        setEpIds([])
        setCharStr('')
        setEpStr('')
        intersection()
    }
    
    return (
        <main className="flex min-h-screen px-12 py-12">
            <div className="w-1/4">
                <h2 className="font-bold sm:text-2xl line-clamp-2 mb-2">Filters</h2>
                <label className="block">Character Name</label>
                <input
                    type="text"
                    id="char-search"
                    name="char-search"
                    onChange={e => setCharStr(e.target?.value)}
                    placeholder="Search Character Name"
                    className="mb-2 w-64 block rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
                <label className="block">Episode Name</label>
                <input
                    type="text"
                    id="ep-search"
                    name="ep-search"
                    onChange={e => setEpStr(e.target?.value)}
                    placeholder="Search Episode Titles"
                    className="mb-2 w-64 block rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
                <button onClick={ clear } className="mb-2 w-64 block rounded-md border border-[#e0e0e0] bg-slate-800 py-3 px-6 text-base font-medium text-white outline-none focus:border-[#6A64F1] focus:shadow-md">Clear Filters</button>
            </div>
            <div className="w-3/4">
                <input
                    type="text"
                    id="search"
                    name="search"
                    onChange={e => setStr(e.target?.value)}
                    placeholder="Search Location Name"
                    className="w-1/3 mb-2 rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
                <h2 className="font-bold sm:text-2xl line-clamp-2 mb-2">Locations</h2>
                { visible && <Location ids={ _ids } location={selected} visible={visible} toggle={toggle} /> }
                {
                    loading ? (
                        <Loader />
                    ) : error ? (
                        <div className="w-full">
                            <h3>{error?.message}</h3>
                        </div>
                    ) : (Array.isArray(data) ? data : data?.results)?.length === 0 ? (
                        <div className="w-full">
                            <h3>No locations were found</h3>
                        </div>
                    ) : (
                        <div className="grid grid-cols-1 mb-2 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4 gap-4">
                            {
                                (Array.isArray(data) ? data : data?.results)?.map((res: LocationData) => (
                                    <div key={res.name} onClick={() => select(res)} className="cursor-pointer bg-white rounded-lg border p-4">
                                        <div className="px-1 py-4">
                                            <div className="font-bold text-xl mb-2">{res?.name} ({res?.type})</div>
                                            <p className="text-gray-700 text-base">Type: {res?.type}</p>
                                            <p className="text-gray-700 text-base">Dimension: {res?.dimension}</p>
                                            <p className="text-gray-700 text-base">{res?.residents?.length} { pluralize('Resident', res?.residents?.length) }</p>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    )
                }
                <Pagination page={page} pages={pages} navigate={setPage} />
            </div>
        </main>
    )
}

export default Home
