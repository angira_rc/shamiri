import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useParams } from 'next/navigation'
import { AiOutlineArrowLeft } from 'react-icons/ai'
import Tabs, { Tab, TabList, TabPanel } from '@atlaskit/tabs'

import { useFetch } from '@/hooks'
import Loader from '@/components/loader'

const Character: React.FC = () => {
    const params = useParams<{ id: string }>()
    
    const [ids, setIds] = useState<string>('')
    const [notes, setNotes] = useState<string>('')

    const episodes = useFetch(`/episode/${ids}`, { method: 'GET' }, true)

    const { data, loading, error } = useFetch(`/character/${params?.id}`, { method: 'GET' }, !params?.id, resp => {
        let _ids = resp?.episode?.map((ep: string) => {
            const parts = ep.split('/')
            return parts[parts.length - 1]
        })?.join(',')

        if (_ids)
            setIds(_ids)
    })

    useEffect(() => {
        episodes.execute()
        return () => { }
    }, [ids])

    if (loading)
        return <Loader />

    if (error)
        return (
            <div className="w-full">
                <h3>{error?.message}</h3>
            </div>
        )

    const getNotes = () => {
        const _notes = localStorage.getItem(`notes-${params?.id}`)
        setNotes(_notes || '')
    }

    const save = () => {
        localStorage.setItem(`notes-${params?.id}`, notes)
        alert('Notes have been saved')
    }

    return (
        <main className="flex min-h-screen flex-row px-12 py-12">
            <div className="w-1/4 p-2">
                <div className="bg-white rounded-lg border p-4">
                    <img src={data?.image} alt="Placeholder Image" className="w-full h-48 rounded-md object-cover" />
                    <div className="px-1 py-4">
                        <div className="font-bold text-xl mb-2">{data?.name} ({data?.status})</div>
                        <p className="text-gray-700 text-base">Species: {data?.species}</p>
                        <p className="text-gray-700 text-base">Gender: {data?.gender}</p>
                        <p className="text-gray-700 text-base">Origin: {data?.origin?.name}</p>
                        <p className="text-gray-700 text-base">Location: {data?.location?.name}</p>
                    </div>
                </div>
            </div>
            <div className="w-3/4 p-2">
                {
                    episodes?.loading ? (
                        <Loader />
                    ) : episodes?.error ? (
                        <div className="w-full">
                            <h3>{episodes?.error?.message}</h3>
                        </div>
                    ) : (Array.isArray(episodes?.data) ? episodes?.data : [episodes?.data]).length === 0 ? (
                        <div className="w-full">
                            <h3>No episodes were found</h3>
                        </div>
                    ) : (
                        <div className="bg-white rounded-lg border w-full p-4">
                            <Link href="/"><span className="text-black-500 text-lg font-semibold flex items-center"><AiOutlineArrowLeft className="mr-2" /> Back</span></Link>
                            <Tabs
                                id="default"
                                onChange={ getNotes }>
                                <TabList>
                                    <Tab>Info</Tab>
                                    <Tab>Notes</Tab>
                                </TabList>
                                <TabPanel>
                                    <div className="w-full">
                                        <div className="font-bold text-xl my-2">Episodes</div>
                                        <div className="grid grid-cols-1 mt-2 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
                                            {(Array.isArray(episodes?.data) ? episodes?.data : [episodes?.data])?.map((ep, i) => (
                                                <div key={i} className="bg-white rounded-lg border p-4">
                                                    <div className="px-1 py-4">
                                                        <div className="font-bold text-xl mb-2">{ep?.episode}</div>
                                                        <p className="text-gray-700 text-base">Episode {ep?.name}</p>
                                                        <p className="text-gray-700 text-base">Aired on {ep?.air_date}</p>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className="w-full">
                                        <div className="font-bold text-xl my-2">Notes</div>
                                        <textarea
                                            id="notes"
                                            name="notes"
                                            value={ notes }
                                            rows={ 5 }
                                            onChange={e => setNotes(e.target?.value)}
                                            placeholder="Enter Notes"
                                            className="w-1/3 mb-2 rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                                        />
                                        <button onClick={ save } className="mb-2 w-64 block rounded-md border border-[#e0e0e0] bg-slate-800 py-3 px-6 text-base font-medium text-white outline-none focus:border-[#6A64F1] focus:shadow-md">Save</button>
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </div>
                    )
                }
            </div>
        </main>
    )
}

export default Character