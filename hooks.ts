import { useState, useEffect, useCallback } from "react"
import { ErrorCallback, FetchResult, Options, SuccessCallback } from "@/types"

let url: string = "https://rickandmortyapi.com/api"

export const useFetch = (
    endpoint: string,
    options: Options = { method: 'GET' },
    manual: boolean = false,
    onSuccess: SuccessCallback = () => null,
    onError: ErrorCallback = () => null
): FetchResult => {
    const [data, setData] = useState<any>(null)
    const [loading, setLoading] = useState<boolean>(false)
    const [error, setError] = useState<Error | null>(null)

    const execute = async () => {
        setLoading(true)

        try {
            const response = await fetch(`${url}/${endpoint}`, options)
            if (!response.ok) 
                throw new Error(response.statusText)

            const json = await response.json()

            setLoading(false)
            setData(json)
            setError(null)
            onSuccess(json)
        } catch (error) {
            setError(error as Error)
            setLoading(false)
            onError(error as Error)
        }
    }

    useEffect(() => {
        if (!manual)
            execute()
    }, [endpoint])

    return { data, loading, error, execute }
}

export const useToggle = (initial: boolean = false): [boolean, () => void, React.Dispatch<React.SetStateAction<boolean>>] => {
    const [state, setState] = useState<boolean>(initial)

    const toggle = useCallback(() => setState(state => !state), [])

    return [state, toggle, setState]
}