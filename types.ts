interface CharacterData {
    id: number
    name: string
    status: string
    species: string
    gender: string
    origin: { name: string }
    location: { name: string }
    image: string
    episode: string[]
}

export interface EpisodeData {
    id: number
    episode: string
    name: string
    air_date: string
    location: {
        url: string
    }
}

export interface LocationData {
    name: string
    type: string
    dimension: string
    residents: any[] // Assuming it's an array of any type
}

export interface Options {
    method?: string,
    headers?: any
    data?: any
}

export type SuccessCallback = (data: any) => void
export type ErrorCallback = (error: Error) => void

export interface FetchResult {
    data: any
    loading: boolean
    error: Error | null
    execute: () => Promise<void>
}