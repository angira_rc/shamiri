import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai"

interface PaginationProps {
    page: number
    pages: number
    navigate: (pageNumber: number) => void
}

const Pagination: React.FC<PaginationProps> = ({ page, pages, navigate }) => {      
    return (
        <nav>
            <ul className="flex">
                <li>
                    <span
                        className="cursor-pointer mx-1 flex h-9 w-9 items-center justify-center rounded-full border border-blue-gray-100 bg-white p-0 text-sm text-blue-gray-500 transition duration-150 ease-in-out hover:bg-light-300"
                        onClick={ () => navigate(1) }
                        aria-label="Previous">
                        <AiOutlineLeft />
                    </span>
                </li>
                {
                    Array.from({ length: pages }, (_, index) => index + 1).map(num =>
                        <li key={ num } onClick={ () => navigate(num) }>
                            <span className={ `cursor-pointer mx-1 flex h-9 w-9 items-center justify-center rounded-full ${ page === num ? 'bg-pink-500 text-white' : 'bg-white' } p-0 text-sm  shadow-md transition duration-150 ease-in-out` }>
                                { num }
                            </span>
                        </li>
                    )
                }
                <li>
                    <span
                        className="cursor-pointer mx-1 flex h-9 w-9 items-center justify-center rounded-full border border-blue-gray-100 bg-white p-0 text-sm text-blue-gray-500 transition duration-150 ease-in-out hover:bg-light-300"
                        onClick={ () => navigate(Math.ceil(pages)) }
                        aria-label="Next">
                        <AiOutlineRight />
                    </span>
                </li>
            </ul>
        </nav>
    )
}

export default Pagination