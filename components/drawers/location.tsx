import Link from 'next/link'
import Drawer from '@atlaskit/drawer'
import { useEffect, useState } from 'react'

import Loader from '../loader'
import { useFetch } from '@/hooks'
import { LocationData } from '@/types'

interface Props {
    ids: string
    location: LocationData
    visible: boolean
    toggle: () => void
}

const Location: React.FC<Props> = ({ ids, location, visible, toggle }) => {
    const [str, setStr] = useState<string>('')

    const { data, loading, error, execute } = useFetch(`/character/${ids}`)

    return (
        <Drawer
            width="extended"
            label={location?.name}
            onClose={toggle}
            isOpen={visible}>
            <h2 className="font-bold sm:text-2xl line-clamp-2 mb-2">{location?.name}</h2>
            <input
                type="text"
                name="search"
                onChange={e => setStr(e.target.value)}
                id="search"
                placeholder="Search Character Name"
                className="w-1/3 rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
            {loading ? (
                <Loader />
            ) : error ? (
                <div className="w-full">
                    <h3>{error.message}</h3>
                </div>
            ) : data?.length === 0 ? (
                <div className="w-full">
                    <h3>No residents were found</h3>
                </div>
            ) : (
                <div className="grid grid-cols-1 mt-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4 pr-10">
                    {(Array.isArray(data) ? data : [data])
                        ?.filter(res => res?.name?.toLowerCase()?.includes(str?.toLowerCase()))
                        ?.map(res => (
                            <Link href={`/character/${res?.id}`} key={res?.id}>
                                <div className="bg-white rounded-lg border p-4">
                                    <img src={res?.image} alt={res?.name} className="w-full h-48 rounded-md object-cover" />
                                    <div className="px-1 py-4">
                                        <div className="font-bold text-xl mb-2">{res?.name} ({res?.status})</div>
                                        <p className="text-gray-700 text-base">Species: {res?.species}</p>
                                        <p className="text-gray-700 text-base">Gender: {res?.gender}</p>
                                        <p className="text-gray-700 text-base">Origin: {res?.origin?.name}</p>
                                        <p className="text-gray-700 text-base">Location: {res?.location?.name}</p>
                                    </div>
                                </div>
                            </Link>
                        ))}
                </div>
            )}
        </Drawer>
    )
}

export default Location
