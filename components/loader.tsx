const { ClipLoader } = require("react-spinners")

const Loader = () => {
    return (
        <div className="flex w-full h-32">
            <ClipLoader />
        </div>
    )
}

export default Loader